const express = require("express");
const history = require("connect-history-api-fallback");
const path = require("path");
const app = express();

const staticFileMiddleware = express.static(
  path.join(__dirname, "./contador-front/dist")
);

app.use(staticFileMiddleware);
app.use(
  history({
    disableDotRule: true,
    verbose: true,
  })
);
app.use(staticFileMiddleware);

const port = 3000;
const server = app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
});

var counter = 0; //Valor inicial del contador
const io = require("socket.io")(server);

io.on("connection", function (socket) {
  console.log("usuario conectado");

  //cuando un usuario se conecta, envia el valor de los click contados
  socket.emit("click_count", [counter]);

  //cuando un usuario realiza click en el boton
  socket.on("clicked", function (data) {
    console.log("clicked  ");
    counter += 1; //Incrementa la cuenta de clicks

    io.emit("click_count", [counter, data]); //Envia a todos los usuarios el valor del contador de clicks
  });
});

import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Contador from "../views/Contador.vue";
import Formulario from "../views/Formulario.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Formulario",
    component: Formulario,
  },
  {
    path: "/contador",
    name: "Contador",
    component: Contador,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
